﻿using CloudWorks.Store.BaseClasses;
using CloudWorks.Store.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Reflection;
using System.ComponentModel.DataAnnotations;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace CloudWorks.Store
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.Unicode; 
            Console.InputEncoding = System.Text.Encoding.Unicode; 

            int seldCheckoutCount = 0;
            while (seldCheckoutCount == 0)
            {
                Console.WriteLine("Введіть загальну кількість кас самообслуговування в магазині");
                int.TryParse(Console.ReadLine(), out seldCheckoutCount);
            }

            /* Filling Persons */
            
            Person[] persons;
            persons = FillPersons();
            
            /*******************/

            var store = new Classes.Store(seldCheckoutCount, persons);

            foreach (var person in persons)
            {
                person.Print();
            }

            store.Print();

            Console.WriteLine($"Загальний час обслуговування: {store.Checkout()}");

            Stopwatch stopWatch1 = new Stopwatch();
            stopWatch1.Start();
            for (int i = 0; i < 1000; i++)
            {
                ReturnListPersons();
            }
            stopWatch1.Stop();

            TimeSpan ts1 = stopWatch1.Elapsed;

            string elapsedTime1 = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                ts1.Hours, ts1.Minutes, ts1.Seconds,
                ts1.Milliseconds / 10);
            Console.WriteLine("RunTime directly" + elapsedTime1);
            

            Type testType = typeof(Program);
            object testInstance = Activator.CreateInstance(testType);

            MethodInfo toInvoke = testType.GetMethod("ReturnListPersons");
           
            Stopwatch stopWatch2 = new Stopwatch();
            stopWatch2.Start();
            for (int i = 0; i < 1000; i++)
            {
                toInvoke.Invoke(testInstance, null);
            }
            stopWatch2.Stop();
            // Get the elapsed time as a TimeSpan value.
            TimeSpan ts2 = stopWatch2.Elapsed;

            // Format and display the TimeSpan value.
            string elapsedTime2 = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                ts2.Hours, ts2.Minutes, ts2.Seconds,
                ts2.Milliseconds / 10);
            Console.WriteLine("RunTime within the reflection" + elapsedTime2);
            //Output json
            Console.WriteLine(JSerialize(persons));
        }

        public static string JSerialize(Person[] persons)
        {
            string serialized = JsonSerializer.Serialize(persons, new JsonSerializerOptions { WriteIndented=true});
            return serialized;
        }
        public static List<Person> ReturnListPersons()
        {
            var persons = FillPersons().ToList();
            return persons;
        }
        private static Person[] FillPersons()
        {
            int personsCount = 0;
            while (personsCount == 0)
            {
                Console.WriteLine("Введіть загальну кількість людей в магазині");
                int.TryParse(Console.ReadLine(), out personsCount);
            }
            var persons = new Person[personsCount];

            for (var i = 0; i < personsCount; i++)
            {
                Console.WriteLine("Введіть ім'я");
                var name = Console.ReadLine();

                Console.WriteLine("Якщо ви хочете додати працівника введіть \"1\". Якщо ви хочете додати покупця введіть \"2\"");
                var personType = Console.ReadLine();

                if (personType == "1")
                {
                    Console.WriteLine($"Швидкість продажу працівником відрізняється від {Employee.DefaultCheckoutSpeed}? т(так)/н(ні)");
                    var isCheckoutSpeedDifferent = Console.ReadLine();
                    if (isCheckoutSpeedDifferent == "т")
                    {
                        Console.WriteLine("Введіть швидкість продажу співробітником");
                        var speedStr = Console.ReadLine();
                        if (double.TryParse(speedStr, out var speed))
                        {
                            persons[i] = new Employee(name, speed);
                        }
                        else
                        {
                            Console.WriteLine("Ви ввели не вірні дані. Повторіть ввід.");
                            i--;
                            continue;
                        }
                    }
                    else if (isCheckoutSpeedDifferent == "н")
                    {
                        persons[i] = new Employee(name);
                    }
                    else
                    {
                        Console.WriteLine("Ви ввели не вірні дані. Повторіть ввід.");
                        i--;
                        continue;
                    }
                }
                else if (personType == "2")
                {
                    Console.WriteLine($"Швидкість покупки клієнтом відрізняється від {Client.DefaultCheckoutSpeed}? т(так)/н(ні)");
                    var isCheckoutSpeedDifferent = Console.ReadLine();
                    if (isCheckoutSpeedDifferent == "т")
                    {
                        Console.WriteLine("Введіть швидкість покупки клієнтом");
                        var speedStr = Console.ReadLine();
                        if (double.TryParse(speedStr, out var speed))
                        {
                            var client = new Client(name, speed);
                            client.Items = ReadProductList().ToArray();
                            persons[i] = client;
                        }
                        else
                        {
                            Console.WriteLine("Ви ввели не вірні дані. Повторіть ввід.");
                            i--;
                            continue;
                        }
                    }
                    else if (isCheckoutSpeedDifferent == "н")
                    {
                        var client = new Client(name);
                        client.Items = ReadProductList().ToArray();
                        persons[i] = client;
                    }
                    else
                    {
                        Console.WriteLine("Ви ввели не вірні дані. Повторіть ввід.");
                        i--;
                        continue;
                    }
                }
                else
                {
                    Console.WriteLine("Ви ввели не вірні дані. Повторіть ввід.");
                    i--;
                    continue;
                }
            }
            //Validation Name Length
            for(int i = 0; i < persons.Length; i++)
            {
                var results = new List<ValidationResult>();
                var context = new ValidationContext(persons[i]);
                if (!Validator.TryValidateObject(persons[i], context, results, true))
                {
                    foreach (var error in results)
                    {
                        Console.WriteLine(error.ErrorMessage);
                    }
                }
            }
           
            return persons;
        }

        public static IEnumerable<Product> ReadProductList()
        {
            var choise = "т";
            while (choise == "т")
            {
                Console.WriteLine("Введіть назву продукта");
                yield return new Product { Name = Console.ReadLine() };
                Console.WriteLine("Продовжити заповнення корзини? т(так)");
                choise = Console.ReadLine();
            }
        }
    }
}
