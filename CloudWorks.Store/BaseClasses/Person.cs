﻿using CloudWorks.Store.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace CloudWorks.Store.BaseClasses
{
    public abstract class Person : IPrintable
    {
        protected Person(string name, double checkoutSpeed)
        {
            Name = name;
            CheckoutSpeed = checkoutSpeed;
        }
        [StringLength(15, MinimumLength = 3)]
        public string Name { get; private set; }

        public double CheckoutSpeed { get; private set; }
        public abstract void Print();
    }
}
